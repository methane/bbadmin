import flask
from flask.ext.googleauth import GoogleFederated

app = flask.Flask(__name__)
app.config.from_object('bbadmin.settings')

auth = GoogleFederated('klab.com', app)

from .views import bp
app.register_blueprint(bp)
