import flask
from bbadmin import app, auth
import os
import requests
import urlparse
from requests_oauthlib import OAuth1

bp = flask.Blueprint('bbadmin', __name__)


def save_access_token(token, secret):
    with open(os.path.join(app.root_path, 'data/access_token'), 'wb') as f:
        f.write(token + ':' + secret)


def get_access_token():
    with app.open_resource('data/access_token') as f:
        return f.read().strip().decode('ascii').split(':')


def makeauth(*args, **kw):
    return OAuth1(app.config['CONSUMER_KEY'],
                  app.config['CONSUMER_SECRET'],
                  *args,
                  **kw)


@app.template_filter()
def nl2br(value):
    return u'<br>'.join(value.split(u'\n'))


@bp.before_request
@auth.required
def before_request():
    flask.g.is_admin = flask.g.user['email'] in app.config['ADMIN_EMAILS']


@bp.route('/')
def index():
    return flask.render_template('index.html')


@bp.route('/register', methods=['POST'])
def register_user():
    email = flask.request.form['email']
    endpoint = ('https://api.bitbucket.org/1.0/groups/klab/klab-jp/members/' +
                email + '/')
    r = requests.put(endpoint, auth=makeauth(*get_access_token()),
                     data=b'', headers={'Content-Length': '0'})
    if r.ok:
        app.logger.info('Success to registering %s', email)
        flask.flash('Registerd ' + email)
    else:
        app.logger.error('Failed to registering %s\n%s', email, r.text)
        flask.flash('Failed %d %s' % (r.status_code, r.text))
    return flask.redirect(flask.url_for('.index'))


@bp.route('/update-token')
def get_request_token():
    callback_uri = flask.url_for('.callback', _external=True).decode('ascii')
    r = requests.post('https://bitbucket.org/!api/1.0/oauth/request_token',
                      auth=makeauth(callback_uri=callback_uri))
    r.raise_for_status()
    res = dict(urlparse.parse_qsl(r.text))
    flask.session['oauth_token'] = oauth_token = res['oauth_token']
    flask.session['oauth_token_secret'] = res['oauth_token_secret']
    return flask.redirect('https://bitbucket.org/!api/1.0/oauth/authenticate' +
                          '?oauth_token=' + oauth_token)


@bp.route('/callback')
def callback():
    verifier = flask.request.args['oauth_verifier']
    auth = makeauth(flask.session['oauth_token'],
                    flask.session['oauth_token_secret'],
                    verifier=verifier)
    r = requests.post(u'https://bitbucket.org/!api/1.0/oauth/access_token',
                      auth=auth)
    r.raise_for_status()
    res = dict(urlparse.parse_qsl(r.text))
    save_access_token(res['oauth_token'], res['oauth_token_secret'])
    flask.flash('Update access token')
    return flask.redirect(flask.url_for('.index'))
